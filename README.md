# hp-pico

- [pico getting started](https://projects.raspberrypi.org/en/projects/get-started-pico-w/0)
- [pico docs](https://picozero.readthedocs.io/en/latest/index.html)

## Simple RF M4 Receiver

- [store link](https://www.adafruit.com/product/1096)
- [datasheet](https://cdn-shop.adafruit.com/datasheets/PT2272.pdf)
