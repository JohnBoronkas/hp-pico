from picozero import LED, DigitalInputDevice, pinout
from time import sleep

pinout()

class TiedOutput:
    def __init__(self, digitalIn, digitalOut):
        self._in = digitalIn
        self._out = digitalOut
        
    def act(self):
        if (self._in.is_active):
            self._out.on()
        else:
            self._out.off()

links = []

a_red    = TiedOutput(DigitalInputDevice(3), LED(15))
links.append(a_red)

b_green  = TiedOutput(DigitalInputDevice(2), LED(14))
links.append(b_green)

c_blue   = TiedOutput(DigitalInputDevice(1), LED(13))
links.append(c_blue)

d_yellow = TiedOutput(DigitalInputDevice(0), LED(12))
links.append(d_yellow)

while(True):
    for link in links:
        link.act()
        
    sleep(0.25)
